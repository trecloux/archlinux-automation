#!/bin/bash
action=$(echo -e "lock\nsleep\nshutdown\nreboot\nlogout" | rofi -dmenu -p "power:")

if [[ "$action" == "lock" ]]
then
    i3lock-fancy-rapid 5 5
fi

if [[ "$action" == "sleep" ]]
then
    systemctl suspend
#    systemctl hybrid-sleep
fi

if [[ "$action" == "logout" ]]
then
    i3-msg exit
fi

if [[ "$action" == "shutdown" ]]
then
    shutdown now
fi

if [[ "$action" == "reboot" ]]
then
    reboot
fi
