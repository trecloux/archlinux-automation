;=====================================================
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;=====================================================

[settings]
wlan_interface = ${env:WLAN_INTERFACE}
eth_interface = ${env:ETH_INTERFACE}

[colors]
background = #000000
foreground = #eceff4
background-alt = #1d2021
foreground-alt = #81a1c1
blk = ${xrdb:color0}
red = ${xrdb:color1}
grn = ${xrdb:color2}
ylw = #ebcb8b
blu = ${xrdb:color4}
mag = ${xrdb:color5}
cyn = ${xrdb:color6}
wht = ${xrdb:color7}
bblk = ${xrdb:color8}
bred = ${xrdb:color9}
bgrn = ${xrdb:color10}
bylw = ${xrdb:color11}
bblu = ${xrdb:color12}
bmag = ${xrdb:color13}
bcyn = ${xrdb:color14}
bwht = ${xrdb:color15}

primary = ${colors.ylw}
secondary = ${colors.cyn}
alert = #bf616a

[global/wm]
margin-top = 5
margin-bottom = 5

[settings]
; The throttle settings lets the eventloop swallow up til X events
; if they happen within Y millisecond after first event was received.
; This is done to prevent flood of update event.
;
; For example if 5 modules emit an update event at the same time, we really
; just care about the last one. But if we wait too long for events to swallow
; the bar would appear sluggish so we continue if timeout
; expires or limit is reached.
;throttle-output = 5
;throttle-output-for = 10

; Time in milliseconds that the input handler will wait between processing events
;throttle-input-for = 30

; Reload upon receiving XCB_RANDR_SCREEN_CHANGE_NOTIFY events
screenchange-reload = true

[bar/primary]
monitor = ${env:MONITOR}
monitor-strict = true
dpi = 72
bottom = false
width = 100%
height = 20
offset-x = 0
offset-y = 0

background = ${colors.background}
foreground = ${colors.foreground}

overline-size = 0
overline-color = #f00
underline-size = 2
underline-color = ${colors.background-alt}

border-bottom-size = 0
border-bottom-color = ${colors.background}

spacing = 1
padding-left = 0
padding-right = 2
module-margin-left = 1
module-margin-right = 2

font-0 = FiraCode Nerd Font:size=14;2
font-1 = Font Awesome 5 Free:pixelsize=14;2

enable-ipc = true

modules-left = i3
modules-center = eth ethspeedup ethspeeddown wlan wlanspeedup wlanspeeddown cpu memory temperature
modules-right = online updates-arch-combined backlight volume battery date dunst

tray-position = right
tray-padding = 4

[bar/secondary]
monitor = ${env:MONITOR}
monitor-strict = true
dpi = 72
bottom = false
width = 100%
height = 20
offset-x = 0
offset-y = 0

background = ${colors.background}
foreground = ${colors.foreground}

overline-size = 0
overline-color = #f00
underline-size = 2
underline-color = ${colors.background-alt}

border-bottom-size = 0
border-bottom-color = ${colors.background}

spacing = 1
padding-left = 0
padding-right = 2
module-margin-left = 1
module-margin-right = 2

font-0 = FiraCode Nerd Font:size=14;2
#font-1 = Siji:pixelsize=10;3
#font-2 = Font Awesome 6 Free:pixelsize=14;2

modules-left = i3
modules-center =
modules-right = date

tray-position = none
tray-padding = 4
;tray-transparent = true
;tray-background = #0063ff

;wm-restack = bspwm
;wm-restack = i3

;override-redirect = true

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

[module/xwindow]
type = internal/xwindow
label = "   %{F#5b5b5b}%{F-} %title:0:60:...%"

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-underline = ${colors.secondary}
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}

label-layout = %layout%

label-indicator-padding = 2
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}

[module/filesystem-home]
type = internal/fs
interval = 25

mount-0 = /home
label-mounted = "%{F#5b5b5b}%{F-} %percentage_used%%"

[module/dunst]
type = custom/ipc
initial = 1
format-foreground = ${colors.foreground}

hook-0 = echo "%{A1:dunstctl set-paused true && polybar-msg hook dunst 2:}%{A}" &
hook-1 = echo "%{A1:dunstctl set-paused false && polybar-msg hook dunst 1:}%{A}" &

[module/filesystem-slash]
type = internal/fs
interval = 25

mount-0 = /
label-mounted = "%{F#5b5b5b}%{F-} %percentage_used%%"

;label-unmounted = %mountpoint%: not mounted
;label-unmounted-foreground = ${colors.foreground-alt}

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false
strip-wsnumbers = false
pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

label-focused = %name%
label-focused-background = ${colors.background-alt}
label-focused-underline = ${colors.ylw}
label-focused-padding = 2

label-unfocused = %name%
label-unfocused-padding = 2

label-urgent = %name%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-visible = %name%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

[module/mpd]
type = internal/mpd

format-online = " <icon-prev> <icon-stop> <toggle> <icon-next>  <icon-repeat> <icon-random>  <label-song>  <bar-progress>"

label-song-maxlen = 40
label-song-ellipsis = true
label-offline = offline

icon-prev = %{F#5b}%{F-}
icon-seekb = %{F#5b}%{F-}
icon-stop = %{F#5b}%{F-}
icon-play = %{F#5b}%{F-}
icon-pause = %{F#5b}%{F-}
icon-next = %{F#5b}%{F-}
icon-seekf = %{F#5b}%{F-}

icon-random = 
icon-repeat = 

toggle-on-foreground = ${colors.primary}
toggle-off-foreground = #66

bar-progress-width = 10
bar-progress-indicator = |
bar-progress-fill = ─
bar-progress-empty = ─

[module/backlight]
type = internal/backlight
format = <label> <bar>
card = intel_backlight
enable-scroll = true
;   %percentage% (default)
label = ﯦ
; Only applies if <bar> is used
bar-width = 10
bar-foreground = ${colors.foreground}
bar-gradient = false
bar-indicator = │
bar-indicator-font = 2
bar-indicator-foreground = #ff
bar-fill = ─
bar-fill-font = 2
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground-alt}

[module/cpu]
type = internal/cpu
interval = 2
#format = <ramp-coreload>
format-prefix = " "  
format-prefix-foreground = #5b
format-suffix = " %"
;format-underline = #f90000
label = %percentage%
#ramp-coreload-spacing = 1
#ramp-coreload-0 = %{F#ebdbb2}▁%{F-}
#ramp-coreload-1 = %{F#ebdbb2}▁%{F-}
#ramp-coreload-2 = %{F#ebdbb2}▁%{F-}
#ramp-coreload-3 = %{F#d79921}▄%{F-}
#ramp-coreload-4 = %{F#d79921}▅%{F-}
#ramp-coreload-5 = %{F#d79921}▆%{F-}
#ramp-coreload-6 = %{F#cc241d}▇%{F-}
#ramp-coreload-7 = %{F#cc241d}█%{F-}

[module/memory]
type = internal/memory
interval = 2
format = <label>
# format-prefix = 
format-prefix-foreground = #5b
;format-underline = #4bffdc
format-prefix = " "
label = %gb_free%
#bar-used-width = 20
#bar-used-foreground-0 = #5af78e
#bar-used-foreground-1 = #5af78e
#bar-used-foreground-2 = #f3f99d
#bar-used-foreground-3 = #ff5c57
#bar-used-fill = 
#bar-used-empty = 
#bar-used-empty-foreground = #444444
#bar-used-indicator =

[module/wlan]
type = internal/network
interface = ${settings.wlan_interface}
interval = 3.0

format-connected = <label-connected>
format-connected-prefix = " "
format-connected-prefix-foreground = #5b

format-connected-underline = #9f78e1
;format-disconnected-underline = ${self.format-connected-underline}

#label-connected = "%{A1:connman-gtk &:}%essid% %local_ip%%{A}"
label-connected = "%{A1:connman-gtk &:}%essid% %{A}"
label-disconnected = "%{A1:connman-gtk &:}%{A}"
label-disconnected-foreground = #5b

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${colors.foreground-alt}

[module/wlanspeedup]
type = internal/network
interface = ${settings.wlan_interface}
label-connected = "%upspeed:7%"
format-connected = <label-connected>
format-connected-prefix = ""
format-connected-prefix-foreground = #5b

[module/wlanspeeddown]
type = internal/network
interface = ${settings.wlan_interface}
label-connected = "%downspeed:7%"
format-connected = <label-connected>
format-connected-prefix = ""
format-connected-prefix-foreground = #5b

[module/eth]
type = internal/network
interface = ${settings.eth_interface}
interval = 3.0

format-connected-underline = #55aa55
#format-connected-prefix = ""
format-connected-foreground-foreground = ${colors.foreground-alt}
label-connected = ""
#label-connected = %local_ip%

format-disconnected-underline = ${self.format-connected-underline}
label-disconnected = %ifname%
label-disconnected-foreground = ${colors.foreground-alt}

[module/ethspeedup]
type = internal/network
interface = ${settings.eth_interface}
label-connected = "%upspeed:7%"
format-connected = <label-connected>
format-connected-prefix = ""
format-connected-prefix-foreground = #5b

[module/ethspeeddown]
type = internal/network
interface = ${settings.eth_interface}
label-connected = "%downspeed:7%"
format-connected = <label-connected>
format-connected-prefix = ""
format-connected-prefix-foreground = #5b

[module/date]
type = internal/date
interval = 1

date = %d/%m
time =  %H:%M

format-foreground = ${colors.foreground}
label-foreground = ${colors.foreground}

label = %date% %time%

[module/volume]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = ""
#label-volume-foreground = ${colors.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground-alt}
label-muted = muted
label-muted-foreground = ${colors.foreground-alt}

bar-volume-width = 10
bar-volume-foreground-0 = #5af78e
bar-volume-foreground-1 = #5af78e
bar-volume-foreground-2 = #5af78e
bar-volume-foreground-3 = #5af78e
bar-volume-foreground-4 = #5af78e
bar-volume-foreground-5 = #f3f99d
bar-volume-foreground-6 = #ff5c57
bar-volume-gradient = false
bar-volume-indicator = │
bar-volume-indicator-font = 2
bar-volume-indicator-foreground = #ff
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground}

click-right = pavucontrol

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98
;time-format = "%H%{F#92}%M%{F-}"
time-format = %H:%M

label-discharging = %percentage%% %time%

format-charging = <animation-charging> <label-charging>

format-discharging = <ramp-capacity> <label-discharging>

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-foreground = ${colors.foreground}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-foreground = ${colors.foreground}
animation-charging-framerate = 750

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 95

format = <ramp> <label>
format-underline = #f50a4d
format-warn = <ramp> <label-warn>
format-warn-underline = ${self.format-underline}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.alert}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.foreground}

[module/updates-arch-combined]
type = custom/script
exec = ~/.config/polybar/polybar-scripts/updates-pacman-aurhelper.sh
click-left = ~/.config/polybar/polybar-scripts/updates-pacman-aurhelper.sh
interval = 600

[module/system-bluetooth-bluetoothctl]
type = custom/script
exec = ~/polybar-scripts/system-bluetooth-bluetoothctl.sh
tail = true
click-left = ~/polybar-scripts/system-bluetooth-bluetoothctl.sh --toggle &

[module/online]
type = custom/script
exec = timeout 1 ping -c 1 8.8.8.8
interval = 5

format = <label>
format-foreground = #5af78e
label = ""
format-padding = 0

format-fail = <label-fail>
format-fail-foreground = ${colors.alert}
label-fail = ""
format-fail-padding = 0
; vim:ft=dosini
