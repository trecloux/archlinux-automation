#!/bin/bash

lockdir=/tmp/polybar-launch.lock
if mkdir -- "$lockdir"
then    # directory did not exist, but was created successfully
    printf >&2 'successfully acquired lock: %s\n' "$lockdir"
    trap 'rm -r "$lockdir"; exit $?' INT TERM EXIT
else
    printf >&2 'cannot acquire lock, giving up on %s\n' "$lockdir"
    exit 0
fi

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done


for i in $(ip -o link show | awk -F': ' '{print $2}')
do
    link=$(ip -o link show $i)
    
    # loop back
    if [[ $link =~ "LOOPBACK" ]]; then
      continue
    fi
    
    # Docker containers
    if [[ $link =~ "NO-CARRIER*" ]]; then
      continue
    fi
    
    if [[ $i =~ ^wlp.* ]]; then
      export WLAN_INTERFACE=$i
    fi
    
    if [[ $i =~ ^enp.* ]]; then
      export ETH_INTERFACE=$i
    fi
done

for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    if xrandr --query | grep $m | grep 'primary'
    then
        MONITOR=$m polybar primary &
    else
        MONITOR=$m polybar secondary &
    fi
done
echo "Bars launched..."
