syntax on
set number
set noswapfile
set hlsearch
set ignorecase
set incsearch

vnoremap <C-C> :w !xclip -i -sel c<CR><CR>


