source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Enable vi mode
bindkey -v

# Plugins
plugins=(autojump fzf jenv volta kubectl git tig ripgrep gcloud helm vi-mode docker docker-compose)

# Oh my ZSH
source $ZSH/oh-my-zsh.sh

# Local settings
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

export BROWSER='firefox'
export EDITOR='vim'

source /home/tom/.config/broot/launcher/bash/br

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

export PATH=$PATH:'/home/tom/.local/bin'
export PATH="$HOME/.bin:$PATH"

export USE_GKE_GCLOUD_AUTH_PLUGIN=True

export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
