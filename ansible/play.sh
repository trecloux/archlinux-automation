#!/usr/bin/env bash
set -e

ansible-galaxy collection install kewlfft.aur
ansible-playbook -i 127.0.0.1 -c local local.yaml
