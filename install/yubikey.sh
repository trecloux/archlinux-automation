#!/bin/bash
set -e

# read -p "Plug in 1st Yubikey and press <enter>" 
# ykfde-enroll -d /dev/nvme0n1p3 -s 1
read -p "Plug in 2nd Yubikey and press <enter>" 
ykfde-enroll -d /dev/nvme0n1p3 -s 0 -c

sed -i 's/^HOOKS=.*/HOOKS=(base udev autodetect keyboard keymap consolefont modconf block ykfde encrypt lvm2 filesystems fsck)/'  /etc/mkinitcpio.conf 
mkinitcpio -P

