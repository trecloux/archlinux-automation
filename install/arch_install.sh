#!/bin/bash
set -e

WLAN_DEVICE='wlan0'
WLAN_SSID='LetITZen'

# Network
#systemctl start iwd.service
#systemctl start dhcpcd.service
#until iwctl device list | grep $WLAN_DEVICE; do
#	echo "Waiting for $WLAN_DEVICE" 
#	sleep 1
#done
#until iwctl station $WLAN_DEVICE get-networks | grep $WLAN_SSID; do
#	echo "Waiting for $WLAN_SSID" 
#	sleep 1
#done
#iwctl station $WLAN_DEVICE connect $WLAN_SSID
#until ip addr show dev $WLAN_DEVICE | grep 'inet '; do
#	echo "Waiting for IP addr"
#	sleep 1
#done

timedatectl set-ntp true

# Partitions
sfdisk /dev/nvme0n1 < nvme0n1.sfdisk

# LUKS Encryption
cryptsetup luksFormat --type luks2 /dev/nvme0n1p3
cryptsetup --allow-discards --persistent open /dev/nvme0n1p3 archlinux

# BTRFS
mkfs.btrfs --label archlinux --force /dev/mapper/archlinux
rm -r /mnt/target || true
mkdir /mnt/target
mount /dev/mapper/archlinux /mnt/target
btrfs subvolume create /mnt/target/@
btrfs subvolume create /mnt/target/@home
btrfs subvolume create /mnt/target/@snapshots
btrfs subvolume create /mnt/target/@log
btrfs subvolume create /mnt/target/@swap
btrfs subvolume create /mnt/target/@pkg
umount /mnt/target
mount -o ssd,subvol=@ /dev/mapper/archlinux /mnt/target
mkdir -p /mnt/target/{efi,boot,home,var/cache/pacman/pkg,.snapshots,var/log,swap}
mount -o ssd,subvol=@home /dev/mapper/archlinux /mnt/target/home
mount -o ssd,subvol=@snapshots /dev/mapper/archlinux /mnt/target/.snapshots
mount -o ssd,subvol=@log /dev/mapper/archlinux /mnt/target/var/log
mount -o ssd,subvol=@pkg /dev/mapper/archlinux /mnt/target/var/cache/pacman/pkg
mount -o nodatacow,ssd,subvol=@swap /dev/mapper/archlinux /mnt/target/swap

# Prepare EFI partition
mkfs.fat -F32 /dev/nvme0n1p1
mount /dev/nvme0n1p1 /mnt/target/efi

# Prepare boot partition
mkfs.ext4 -F /dev/nvme0n1p2
mount /dev/nvme0n1p2 /mnt/target/boot

# Swap
chmod 0700 /mnt/target/swap
touch /mnt/target/swap/file
chmod 0600 /mnt/target/swap/file
chattr +C /mnt/target/swap/file
dd if=/dev/zero of=/mnt/target/swap/file bs=4MB count=16000
mkswap /mnt/target/swap/file
swapon /mnt/target/swap/file


reflector --country France --protocol https --sort rate --latest 5 --save /etc/pacman.d/mirrorlist
pacman -Sy --noconfirm archlinux-keyring
pacstrap /mnt/target base linux linux-firmware btrfs-progs refind efibootmgr vim networkmanager zsh grml-zsh-config intel-ucode fuse-exfat sudo ansible python-packaging yubikey-full-disk-encryption reflector

genfstab -U /mnt/target >> /mnt/target/etc/fstab

cp post_chroot.sh /mnt/target/root/
arch-chroot /mnt/target /root/post_chroot.sh
rm /mnt/target/root/post_chroot.sh

