#!/bin/bash
set -e

mkdir /mnt/target

cryptsetup --allow-discards --persistent open /dev/nvme0n1p3 archlinux

mount -o ssd,subvol=@ /dev/mapper/archlinux /mnt/target
mkdir -p /mnt/target/{efi,boot,home,var/cache/pacman/pkg,.snapshots,var/log,swap}
mount -o ssd,subvol=@home /dev/mapper/archlinux /mnt/target/home
mount -o ssd,subvol=@snapshots /dev/mapper/archlinux /mnt/target/.snapshots
mount -o ssd,subvol=@log /dev/mapper/archlinux /mnt/target/var/log
mount -o ssd,subvol=@pkg /dev/mapper/archlinux /mnt/target/var/cache/pacman/pkg
mount -o nodatacow,ssd,subvol=@swap /dev/mapper/archlinux /mnt/target/swap

arch-chroot /mnt/target zsh

