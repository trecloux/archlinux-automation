#!/bin/bash
set -e

WLAN_DEVICE='wlan0'
#WLAN_SSID='Pretty Fly for a WiFi'
WLAN_SSID='LetITZen'

# Network
systemctl start iwd.service
systemctl start dhcpcd.service
until iwctl device list | grep $WLAN_DEVICE; do
	echo "Waiting for $WLAN_DEVICE" 
	sleep 1
done
until iwctl station $WLAN_DEVICE get-networks | grep "$WLAN_SSID"; do
	echo "Waiting for $WLAN_SSID" 
	sleep 1
done
iwctl station $WLAN_DEVICE connect "$WLAN_SSID"
until ip addr show dev $WLAN_DEVICE | grep 'inet '; do
	echo "Waiting for IP addr"
	sleep 1
done

