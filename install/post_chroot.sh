#!/bin/bash
set -e

# Time
ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
hwclock --systohc

# Locale
sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' > /etc/locale.conf

# Hostname
echo 'oryx' > /etc/hostname
echo -e "127.0.0.1\tlocalhost" >> /etc/hosts
echo -e "127.0.0.1\toryx" >> /etc/hosts

# Add an yubikey challenge
# read -p "Plug in 1st Yubikey and press <enter>" 
# ykfde-enroll -d /dev/nvme0n1p3 -s 1
# Replace encryption temporary key with yubikey challenge
# read -p "Plug in 2nd Yubikey and press <enter>" 
# ykfde-enroll -d /dev/nvme0n1p3 -s 0 -c

# Initramfs
sed -i 's/^HOOKS=.*/HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt filesystems fsck)/'  /etc/mkinitcpio.conf 
touch /etc/vconsole.conf
mkinitcpio -P

# Enable NetworkManager
systemctl enable NetworkManager.service

# Refind
CRYPT_UUID=$(blkid /dev/nvme0n1p3 | sed 's/.*UUID="\(.*\)" TYPE.*/\1/')
REFIND_LINUX='"Arch Linux"  "cryptdevice=UUID=@UUID@:archlinux:allow-discards rw root=/dev/mapper/archlinux rootflags=subvol=@ initrd=intel-ucode.img initrd=initramfs-linux.img ibt=off'
echo $REFIND_LINUX | sed "s/@UUID@/$CRYPT_UUID/" > /boot/refind_linux.conf
refind-install

#Reflector
reflector --country France --sort rate --protocol https --latest 5 --save /etc/pacman.d/mirrorlist

#Users
chsh -s /bin/zsh
echo "Tom's password"
useradd -m -G wheel -s /bin/zsh tom
passwd tom
echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers
